const gulp = require('gulp');
const sourcemaps = require("gulp-sourcemaps");

// const debug = require("gulp-debug");
// const ts = require('gulp-typescript');
// const tsProject = ts.createProject('tsconfig.json');
const concat = require("gulp-concat");
const changed = require('gulp-changed');
// const uglifyEs = require('uglify-es');
// const uglifyComposer = require('gulp-uglify/composer');

// const uglify = uglifyComposer(uglifyEs, console);

const svgstore = require('gulp-svgstore');
const svgmin = require('gulp-svgmin');

const path = require('path');
const rename = require('gulp-rename');
const replace = require('gulp-replace');

const postcss = require('gulp-postcss');

const paths = {
	template: {
		pcss: 'resources/pcss/*.pcss',
	},
	singles: {
        ts: 'src/ts/**/*.ts',
		pcss: [
			'src/ts/**/*.pcss',
			'src/pcss/components/**/*.pcss',
		]
	},
	bxComponents: {
		ts:[
			'./components/**/*.ts',
			'./components/**/.default/*.ts',
		],
		pcss:[
			'./components/**/*.pcss',
			'./components/**/.default/*.pcss',
		]
	},
	js:[
		'./js/**/*.js',
		'!./js/**/*.min.js',
		'./components/**/*.js',
		'!./components/**/*.min.js',
		'./components/**/.default/*.js',
		'!./components/**/.default/*.min.js'
	],
	//ts:['./js/**/*.ts'],
    // svg: 'src/svg/**/*.svg',
    // svgmin: 'img/svg/*.svg'
};
gulp.task('default', function() {
	// gulp.watch(paths.js, ['js']);
    gulp.watch(paths.template.pcss, gulp.series('template-pcss'));
	// gulp.watch(paths.singles.ts, ['singles-ts']);
    // gulp.watch(paths.singles.pcss, ['singles-pcss']);
	// gulp.watch(paths.bxComponents.ts, ['bxComponents-ts']);
    // gulp.watch(paths.bxComponents.pcss, ['bxComponents-pcss']);
    // gulp.watch(paths.svg, ['svgstore']);
    // gulp.watch(paths.svgmin, ['svgmin']);
});

gulp.task('template-pcss', function () {
    return gulp
        .src(paths.template.pcss, {base: "resources/pcss/template"})
        .pipe(sourcemaps.init())
        .pipe(concat('template.css'))
        .pipe(postcss())
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest('public/css'))
        // .pipe(browserSync.stream())
        ;
});
// gulp.task('singles-ts', function () {
//     return gulp
//         .src(paths.singles.ts, {base: './'})
//         .pipe(sourcemaps.init())
//         .pipe(changed('dist', {extension: '.js', transformPath: (newPath)=> newPath.replace('src/ts', 'js')}))
//         .pipe(tsProject())
//         .pipe(replace(/(import ['|"][^'"]+)(['|"];\s)/g, '$1.js$2'))
// 		.pipe(replace(/(import (?:\*|(?:\{?[^}]+\}?))(?: as \w+)? from ["|'][^'"]+)(["|'];\s)/g, '$1.js$2'))
//         // .pipe(uglify())
//         .pipe(rename((path)=> path.dirname = path.dirname.replace('src/ts', 'js')))
//         .pipe(sourcemaps.write('.'))
//         .pipe(gulp.dest('dist'));
// });
// gulp.task('singles-pcss', function () {
// 	return gulp
// 			.src(paths.singles.pcss, {base: "./"})
// 			.pipe(changed('dist', {
// 			    extension: '.css',
//                 transformPath: (newPath)=>newPath.replace('src/ts', 'js').replace('src/pcss', 'css').replace('.pcss', '.css')
// 			}))
// 			.pipe(sourcemaps.init())
// 			.pipe(postcss())
// 			.pipe(rename((path)=>{
// 				path.dirname = path.dirname.replace('src/ts', 'js').replace('src/pcss', 'css');
//                 path.extname = path.extname.replace('.pcss', '.css');
// 			}))
// 			.pipe(sourcemaps.write('.'))
// 			.pipe(gulp.dest('dist'))
// 		// .pipe(browserSync.stream())
// 		;
// });
// gulp.task('bxComponents-ts', function () {
//     return gulp
//         .src(paths.bxComponents.ts, {base: './'})
//         .pipe(changed('.', {extension: '.js'}))
//         .pipe(sourcemaps.init())
//         .pipe(tsProject())
//         .pipe(replace('src/ts', 'dist/js'))
//         .pipe(replace(/(import ['|"][^'"]+)(['|"];\s)/g, '$1.js$2'))
//         .pipe(replace(/(import (?:\*|(?:\{?[^}]+\}?))(?: as \w+)? from ["|'][^'"]+)(["|'];\s)/g, '$1.js$2'))
//         // .pipe(uglify())
//         .pipe(sourcemaps.write('.'))
//         .pipe(gulp.dest('.'));
// });
// gulp.task('bxComponents-pcss', function () {
//     return gulp
//         .src(paths.bxComponents.pcss, {base: "./"})
//         .pipe(changed('.', {extension: '.css'}))
//         .pipe(sourcemaps.init())
//         .pipe(postcss())
//         .pipe(rename({extname:'.css'}))
//         .pipe(sourcemaps.write('.'))
//         .pipe(gulp.dest('.'))
//         ;
// });
// gulp.task('svgstore', function () {
//   return gulp
//     .src(paths.svg)
//     .pipe(svgmin(function (file) {
//         const prefix = path.basename(file.relative, path.extname(file.relative));
//         return {
//             plugins: [
//             	{
// 					cleanupIDs: {
// 						prefix: prefix + '-',
// 						minify: true
// 					},
//             	},
//                 { convertStyleToAttrs: false },
//                 { inlineStyles: true },
//                 { removeDoctype: true },
//                 { removeTitle: true },
//                 { removeDesc: true },
//                 { removeComments: true }
// 			]
//         };
//     }))
//     .pipe(svgstore())
//   	.pipe(rename('sprite.svg'))
//     .pipe(gulp.dest('dist'));
// });
// gulp.task('svgmin', function () {
//   return gulp
//     .src(paths.svgmin)
//     .pipe(svgmin({
//         plugins: [
//         	{ removeDoctype: true },
//         	{ removeTitle: true },
//         	{ removeDesc: true },
// 			{ removeComments: true }
// 		]
//     }))
//     .pipe(gulp.dest('img/svg/opt'));
// });