const path = require('path');
const paths = require('postcss-url/src/lib/paths');
const normalize = paths.normalize;
// const getAssetsPath = paths.getAssetsPath;
const dist = path.resolve(__dirname, 'public/css');
const plugins = {
    'postcss-import':{},
    'postcss-url':{url: (asset, dir, options)=>{
            // console.log(dir);
            // console.log(dest);
            // const dest = getAssetsPath(dir.to, options && options.assetsPath || '');
            const rebasedUrl = normalize(
                path.relative(dist, asset.absolutePath)
            );
            // console.log(asset);
            // console.log(rebasedUrl);
            // console.log(dist)
            return rebasedUrl;
        }
        // baseDir: path.resolve(__dirname, '.')
    },
    // 'postcss-url': {url: "rebase"},
            // 'postcss-pseudoelements':{},
    'postcss-for':{},
    'postcss-each':{},
    'postcss-extend-rule':{},
    'postcss-nested-ancestors':{},
    'postcss-nested':{},
    // 'postcss-custom-media':{},
    // 'postcss-custom-selectors': {},
    'postcss-preset-env':{
        browsers: ['> 2% in RU', 'last 1 Safari versions'],
        stage: 0,
        features: {
            'nesting-rules': true
            // nesting: false,
            // customProperties: false
            // customProperties: {preserve:true}
        },
        warnForDuplicates: false
    },
};
// if(process.env.NODE_ENV==='production'){
//     plugins['css-mqpacker'] = {};
//     plugins['cssnano'] = {};
// }
// console.log(plugins);
module.exports = {
    plugins: plugins
};